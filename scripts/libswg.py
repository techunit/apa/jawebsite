#!/usr/bin/python3
# coding: utf-8

from string import Template
from pathlib import Path
from bs4 import BeautifulSoup

def extractconfig(s_data):
    """extract config from string, return dictionnary"""
    t_data = s_data.split("\n")
    d_config = {}
    for s_e in t_data:
        if "=" in s_e : # if we got a parameter on this line
            s_key,s_value = s_e.split('=',1)
            s_key = s_key.strip()
            s_value = s_value.strip()
            d_config[s_key]=s_value
    return d_config

def parsemd(s_data):
    """split file into header and content, return header and content"""
    s_trash, s_header, s_content = s_data.split('---',2)
    return removeemptyline(s_header), s_content

def removeemptyline(s_val):
    """remove empty lines in a multilines string"""
    t_lines = s_val.split("\n")
    t_res = []
    for s_line in t_lines:
        s_line = s_line.strip()
        if s_line != "":
            t_res.append(s_line)
    return "\n".join(t_res)

def usetemplate(s_template,d_config):
    """substitute patern by value in dictionnary, return string"""
    template = Template(s_template)
    s_result = template.substitute(d_config)
    return s_result

def buildmenu(s_menu_conf,d_content,page):
    """build menu from menu.conf, d_content, page. return menu as html string"""

    s_menu_conf = removeemptyline(s_menu_conf)
    s_item = '<a href="{}" class="{}item" target="_self"><div class="content">{}</div></a>'
    s_dropdown = '<div class="{}dropdown"><div class="content">{}</div><div class="hidden">{}</div></div>'
    s_dropdown_item = s_item
    #s_item = '<div class="{}item"><a class="content" href="{}" target="_self">{}</a></div>'
    #s_dropdown = '<div class="{}dropdown"><div class="content dropdown-first">{}</div><div class="hidden">{}</div></div>'
    #s_dropdown_item = '<div class="{}item"><a class="content dropdown-element" href="{}" target="_self">{}</a></div>'
    s_active = "active "
    s_notactive = ""


    

    s_current = Path(page).stem

    s_tmp_menu = ""

    print("building menu")

    for s_line_conf in s_menu_conf.split("\n"):
        t_elem_line_conf = s_line_conf.split(":",1)
        if len(t_elem_line_conf) > 1 :
            # it's a dropdown
            s_cat = t_elem_line_conf[0]

            s_tmp_dropdown_item = ""
            t_cat_args = [e.strip() for e in t_elem_line_conf[1].split(",")]
            s_cat_activity = s_notactive
            for cat_arg in t_cat_args:
                s_stem = Path(cat_arg).stem
                if s_stem == s_current :
                    s_activity = s_active
                    s_cat_activity = s_active
                else :
                    s_activity = s_notactive
                s_text = d_content[s_stem]['pagetitle']
                s_url = Path(s_stem).with_suffix('.html')
                s_tmp_dropdown_item += s_dropdown_item.format(s_url,s_activity,s_text)

            s_tmp_menu += s_dropdown.format(s_cat_activity,s_cat, s_tmp_dropdown_item)

        elif len(t_elem_line_conf) == 1 :
            # it's item
            s_stem = Path(t_elem_line_conf[0]).stem
            if s_stem == s_current :
                s_activity = s_active
            else :
                s_activity = s_notactive
            s_text = d_content[s_stem]['pagetitle']
            s_url = Path(s_stem).with_suffix('.html')

            s_tmp_menu += s_item.format(s_url,s_activity,s_text)

        else :
            print('[error] something went wrong')
    return s_tmp_menu

class TemplateAssets(Template):
    delimiter = ':'
    pattern = r'''
    :(?:
    (?P<escaped>:)|
    (?P<named>[_a-z][_a-z0-9]*):|
    (?P<braced>[_a-z][_a-z0-9]*):|
    (?P<invalid>::)
    )
    '''

# same as use template but match :var: instead of ${var}
def useassets(s_template,d_config):
    template = TemplateAssets(s_template)
    s_result = template.substitute(d_config)
    return s_result

# put everything in boxes between 2 doubles lines <hr><hr>
def boxit(s_data):
    #BeautifulSoup(s_data, 'html.parser').prettify()
    s_data = "\n".join([e.strip() for e in s_data.split('\n')])
    t_data = s_data.split("<hr />\n<hr />\n",2)
    if len(t_data) > 1 :
        s_tobox = t_data[1]
        bs_tobox = BeautifulSoup(s_tobox,'html.parser')
        t_tobox = [e for e in bs_tobox.children if e.name]
        s_boxed = ''
        s_boxed += '<div class="root_container">'
        for i in range(len(t_tobox)):
            if t_tobox[i].name == "h2" : #and i+1 < len(t_tobox) and t_tobox[i+1].name =="p" :
                s_boxed += '<div class="question_container">'
                s_boxed += str(t_tobox[i])
                offset = 1
                while i+offset < len(t_tobox) and t_tobox[i+offset].name != "h2" :
                    s_boxed += str(t_tobox[i+offset])
                    offset += 1
                s_boxed += '</div>'
        s_boxed += '<div class="question_container"></div><div class="question_container"></div></div>'
        t_data[1] = s_boxed
        #return BeautifulSoup("\n".join(t_data), 'html.parser').prettify()
        return "\n".join(t_data)
    return s_data

# when use it as a script
if __name__ == '__main__' :
    s = '''

    <h1>Title</h1>
    <hr>
    <hr>
    <h2> test box </h2>
    <p> test content box</p>
    <h2> test box </h2>
    <p> test content box</p>
    <h2> test box </h2>
    <p> test content box</p>
    <hr>
    <hr>
    <h2> test </h2>
    <p> test content </p>
    '''
    print(s)
    print(boxit(s))
