---
pagetitle=Supports
template=index
---

# Ressources
## Liste des supports de Julian Assange
<script>
  function toggle(id) {
    var div = document.getElementById("d-"+id);
    var but = document.getElementById("b-"+id);
    if (div.style.display === "none") {
      div.style.display = "block";
      but.value="hide"
    } else {
      div.style.display = "none";
      but.value="show"
    }
  }
const url = "https://scripts.assangecampaign.org.au/friends?lang=fr";
/* fetch(url, { mode: 'no-cors' }) */
fetch(url)
  .then(response => response.text())
.then(html => {
  // console.log(html);
  document.getElementById('fr-data').innerHTML = html;
})
.catch((err) => console.log( url + " response. Blocked by browser?" + err));
</script>
<div id="fr-data">Campaigns update here</div>
