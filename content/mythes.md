---
pagetitle=Mythes
template=index
---

# Mythes sur l'Affaire Julian Assange

---

---

## Julian assange est un espion.

**FAUX !** Julian Assange est un journaliste et un informaticien, dont le travail a été récompensé par de nombreux prix (Amnesty International, Sam Adams Award, Voltaire Award for Free Speech, Prix de la Paix de Stuttgart, etc.). Il a travaillé en collaboration avec les médias les plus prestigieux du monde afin de révéler des crimes de guerre, des faits d'espionnage de la part des USA, la corruption du système bancaire, et bien d'autres choses encore.

## Wikileaks est une organisation de hackers

**FAUX !** Wikileaks a seulement pour but d'offrir l'anonymat aux lanceurs d'alertes tout en assurant la publication de leurs documents s'ils sont d'utilité publique et si l'on peut assurer leur authenticité.

## Julian Assange a mis des vies en danger

**FAUX !** Lors du procès de Chelsea Manning (l'autrice des fuites des documents « War Logs »), le représentant de l'armée US a admis que, « à sa connaissance », aucune vie n'a été mise en danger.

## Julian Assange est un traître pour les USA

**FAUX !** Julian Assange, en tant que journaliste, n'a fait que publier des informations d'intérêt public. De plus, il est australien et ne peut donc pas avoir trahi un pays qui n'est pas le sien.

## Julian Assange est un voleur

**FAUX !** Julian Assange, tout comme de nombreux journalistes possède des sources qui lui fournissent des informations. L'organisation Wikileaks procède alors à une analyse des données, et décide de publier ce qui relève de l'intérêt du public.


<!-- ## Non, Julian Assange n'est pas un violeur -->

---

---
