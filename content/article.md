---
pagetitle=L'Appel
template=index
---
<!-- 2020 12 11 11:13 -->
# Appel aux journalistes : pourquoi il faut médiatiser l'affaire Assange

À la suite de la tragédie qui coûte la vie à plus de 3 000 personnes aux États-Unis le 11 septembre 2001, une loi anti-terroriste est promulguée : la **loi Patriot Act**. Elle permet la mise en place d'une surveillance électronique généralisée, étendue aux citoyens du monde entier, et la détention arbitraire et illimitée de toute personne soupçonnée de terrorisme.

En février 2003, Colin Powell exhibe de **fausses preuves** devant l’assemblée des Nations-Unies comme prétexte à l'invasion de l'Irak. Si le général et homme politique américain ne parvient pas à abuser la diplomatie française, celle-ci est dans l’incapacité de déconstruire factuellement le piège tendu à l’opinion publique par le complexe militaro-industriel, et ceux que l’on nommera dès lors les néo-conservateurs.

## L’anonymat comme réponse

Un journaliste australien est convaincu que “***Si les guerres peuvent être déclenchées par des mensonges, la paix peut être préservée par la vérité.***” Son nom est Julian Assange.

Il fonde Wikileaks et sa maison d’édition *Sunshine Press*. Le principe est simple : comme la masse des structures de surveillance augmente, la probabilité que des initiés se lèvent pour en dénoncer les dérives s'accroît également. Il s’agit donc pour lui de recueillir des informations et d’en vérifier l’authenticité et l'intérêt pour le public.

Le cryptographie permet d’anonymiser les navigations sur Internet et Julian Assange est lui-même l’auteur de plusieurs brevets. **En alliant les compétences d'informaticiens et de journalistes, Wikileaks garantit à ses sources l'anonymat et assure un traitement journalistique du big data**.

La réputation de sérieux de la plateforme attire les lanceurs d’alerte du monde entier. L’authentification de plus de 10 millions de documents, en partenariat avec les plus grandes rédactions internationales, prouve
la validité du concept de Wikileaks et lui vaut une trentaine de distinctions journalistiques.

![](img/IFJ.png)

>[L’International Federation of Journalists dénonce la persécution de
Julian Assange par la Grande-Bretagne et les Etats-Unis - ifj.org](https://www.ifj.org/media-centre/news/detail/category/press-releases/article/le-crime-dassange-denoncer-les-atrocites-des-autres.html)
>
> *L’IFJ rappelle que Julian Assange est membre de la Media Entertainment
and Arts Alliance, l'affiliée de la FIJ en Australie, il est donc par extension
adhérent de la FIJ et détenteur de la Carte de presse
internationale (CPI).*

## L'intérêt du public

Wikileaks connaît son apogée en 2010 **quand les circonstances de la mort de 2 journalistes de Reuters à Bagdad sont élucidées par la publication de la vidéo “Collateral Murder”**, déclarée “perdue” 3 ans plus tôt par l’armée américaine. Les rires et insultes des soldats, tirant depuis un hélicoptère sur les journalistes, les personnes leur portant secours, et achevant les blessés, choquent profondément une partie de l’opinion publique.

C’est un officier du renseignement, Chelsea Manning, qui fuite ce document ultra-sensible à Wikileaks, ainsi que les **400 000 documents publiés quelques mois plus tard, répertoriant l’ensemble des crimes de guerre en Irak et révélant le nombre exact de victimes civiles entre 2004 et 2009, les “Iraq and Afghanistan War Logs”**.

Notons que le *New York Times* et le *Washington Post*, initialement contactés par le lanceur d’alerte, avaient refusé de publier les documents et que - exception remarquable ! -  Chelsea Manning est arrêtée par les renseignements de l'armée US.

Mais les néo-conservateurs connaissent déjà Wikileaks. En 2008, la plateforme publie les documents internes d’une des prisons secrètes de la CIA : **Les célèbres “Guantanamo Files”, documentant la torture de 800 détenus de 14 à 87 ans et illustrant “l’archipel du goulag” américain dénoncé plus tôt par Amnesty international**. Ces “oubliettes” permettent l'impensable dans une démocratie : décisions de détention extra-judiciaire et actes de cruauté.

Les révélations “Collateral Murder”, “Iraq war logs” et “Guantanamo files” provoquent l’ire de l’agence étasunienne de renseignement, qui n’a depuis de cesse d’employer ses compétences pour œuvrer à la destruction physique, psychique et médiatique de Julian Assange.

Mais la plateforme de presse numérique ne se laisse pas intimider par la traque de son rédacteur en chef. Alors que Julian Assange est reclus depuis 2012 dans l’ambassade d'Equateur à Londres, **Wikileaks publie en 2016 les documents “NSA World Leaders Targets”. On y apprend la mise sous écoute de responsables politiques européens** tel qu'’Angela Merkel, François Hollande, Nicolas Sarkozy, Pierre Moscovici et François Baroin. Puis en 2017, Wikileaks publie les documents **“Vault 7”, du nom du programme de cyber-armes de la CIA, qui permet l’activation et la commande de n'importe que objet connecté, y compris les téléphones éteints ou les voitures automatiques**.

![](img/nations_unies.jpg)

>["La vie de Julian Assange pourrait être en danger, selon un expert de l’ONU" - news.un.org](https://news.un.org/fr/story/2019/11/1055241)
>
> *Nils Melzer, le Rapporteur spécial des Nations Unies sur la torture, a constaté avec son équipe médicale qu'Assange présentait tous les symptômes d’une exposition prolongée à des tortures psychologiques. Par ailleurs, 117 médecins ont signé une tribune dans The Lancet pour dénonçer “la politisation des principes de la médecine” et exiger que cesse la torture du journaliste.*

## Extraterritorialité

Exhumé pour l’occasion, l’Espionnage Act de 1917 permet au pouvoir judiciaire américain d’étendre son domaine d’action à l’international, afin de mener une guerre sans chars dans les pays “amis”. **Les journalistes étrangers qui publient des documents “hostiles” sur Internet deviennent judiciables aux Etats-Unis !**

La couleuvre étant grosse, il devient nécessaire de discréditer Wikileaks pour créer le précédent judiciaire. La CIA charge donc la société de renseignement Strategic Forecasting Inc, dite “Stratford”, d’orchestrer en sous-traitance la stratégie de destruction médiatique de Wikileaks. **“Faisons croire qu’ils ne protègent pas leurs sources”, peut-on lire dans les documents de l’entreprise aux 100 000 collaborateurs, documents ironiquement fuités à la plateforme en ligne.**

La guerre de l’information n’a plus de limite : Assange est un agent russe, un faux journaliste, un violeur même ! Et un hacker à la tête des Anonymous sans doute. Calomnions ! Il en restera toujours quelque chose…

Luke Harding, journaliste du *Guardian*, participe à la curée d’une manière remarquable. Il invente de toutes pièces une rencontre entre Paul Manafort (un avocat et lobbyiste américain), plusieurs russes et Julian Assange dans l’ambassade d’Equateur. Cet article est ce que tout journaliste intègre réprouve : un “hoax”, une “fake news”. Non corroboré en dépit d’une surveillance qui coûta plus de 10 000 000£ au contribuable britannique, le papier d’Harding est la passe décisive faite à la justice américaine, pour assimiler Wikileaks aux agences de renseignement russes.

Qu’importe la vérité, la loi et la complexité du métier de journaliste.
L’opération vise à l’amalgame et à l’omerta. Le message aux grandes
rédactions est clair, comme dans un film de Coppola : “*Nous vous faisons
une offre que vous ne pouvez pas refuser*”.

**Luke Harding a accepté l’offre depuis longtemps. En 2011, il trahit le
secret professionnel qui l’unit à Wikileaks en publiant le mot de passe
d’accès aux archives de la plateforme dans son livre “Wikileaks, la fin
du secret”...**

Le 4 janvier 2021, sera prononcé l’acte d’extradition de Julian Assange
vers les Etats-Unis. Quittant sa prison anglaise de haute sécurité, le
journaliste passera dans le district oriental de **Virginie, siège de la
CIA - grande pourvoyeuse d’emplois locaux, soit dit en passant - pour y
être jugé par un “grand jury” bourré de conflits d'intérêts**.

Il sera trop tard alors, car l’inculpation au titre de l’Espionnage Act
de 1917 permet un procès à huis-clos et la non-recevabilité des
motivations de l’accusé.

**Pire, l’Espionnage Act de 1917 ouvre la possibilité d’inculper toute
personne ayant participé directement ou indirectement au travail
d’édition.**

Le journaliste sera placé à vie dans une cellule de 6m², au sein d’une prison de
type SuperMax située dans le Colorado. **Des Mesures Administratives
Spéciales (MAS) consistant en un confinement intellectuel seront sa
couronne d’épine : ni télévision, ni radio, ni lecture ; 15 minutes
surveillées d’interaction téléphonique par mois.**

<iframe width="560" height="315" src="https://www.youtube.com/embed/1b2HR4LscuU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!--
[https://www.youtube.com/watch?v=1b2HR4LscuU&feature=youtu.be](https://www.youtube.com/watch?v=1b2HR4LscuU&feature=youtu.be)
-->

> *Avant de devenir Garde des Sceaux, Eric Dupond-Moretti avait requis un entretien avec Emmanuel Macron pour demander à la France d’accorder l’asile politique à Julian Assange. En application des accords Dublin III, l’asile qui serait accordé par la France serait dû par la Grande-Bretagne. Mais il faut agir avant le 31 décembre, date du Brexit !. *

La “Wikileaks Task Force” de la CIA entend finir le travail. Au cours du simulacre de procès en extradition auquel Assange a eu droit, les avocats américains ont déclaré devant la Cour britannique qu’ils se **réservaient le droit d'aller chercher les autres journalistes**. La résolution du Parlement européen de janvier 2020 marque une prise de conscience de l’hostilité judiciaire américaine : **“La détention et les poursuites contre Julian Assange sont un dangereux précédent pour les journalistes”**.

### **Nous vous demandons de couvrir l’affaire Assange. Il en va de l'intérêt du public.**
_

# Contact presse

**Cette page web est issue de la rencontre entre des citoyens-engagés et un citoyen-journaliste : Viktor Dedaj.**

**Viktor Dedaj couvre l’affaire Assange depuis 10 ans sur son blog “legrandsoir.info”.** Pour lui, “*dans les sociétés dites démocratiques, les procès politiques se tiennent dans le silence*”. Il s’étonne humblement, citoyen lambda, d’être devenu un expert français, un porte-parole des citoyens, plutôt que les grands journalistes de métier.

**Vous êtes journaliste et vous avez décidé de couvrir l'affaire Assange avant le Brexit ? Notre équipe vous met en contact avec Viktor Dedaj, journaliste du site Le Grand Soir** :

Téléphone : [+33 7 53 79 69 62](tel:+33753796962)\
E-mail : [journalistepourlapaix@protonmail.com](mailto:journalistepourlapaix@protonmail.com)

## Articles et traductions de Viktor Dedaj :

-   [*Réduire au silence les dissidents : WikiLeaks et la violation des
    droits de l’homme (Sydney Morning
    Herald)*](https://www.legrandsoir.info/reduire-au-silence-les-dissidents-wikileaks-et-la-violation-des-droits-de-l-homme-sydney-morning-herald.html)
-   [*Le traitement infligé à Assange est un test décisif pour le futur
    du journalisme et de la liberté
    d’expression*](https://www.legrandsoir.info/le-traitement-inflige-a-assange-est-un-test-decisif-pour-le-futur-du-journalisme-et-de-la-liberte-d-expression.html) -
    Günter Wallraff
-   [*Procès Assange : le témoignage de Noam
    Chomsky*](https://www.legrandsoir.info/proces-assange-le-temoignage-de-noam-chomski.html)
-   [*« Si Assange est extradé vers les États-Unis, alors aucun
    journaliste dans le monde n’est à l’abri » : Daniel
    Ellsberg*](https://www.legrandsoir.info/si-assange-est-extrade-vers-les-etats-unis-alors-aucun-journaliste-dans-le-monde-n-est-a-l-abri-daniel-ellsberg.html)
-   [*\#Unity4J : Christine Assange lance un appel
    d’urgence*](https://www.legrandsoir.info/unity4j-christine-assange-lance-un-appel-d-urgence.html)
-   [*Retour sur l’étrange et persistante désinformation autour de «
    l’affaire » Julian Assange (le Fake News dans toute sa
    splendeur)*](https://www.legrandsoir.info/retour-sur-l-etrange-et-persistante-desinformation-autour-de-l-affaire-julian-assange-le-fake-news-dans-toute-sa-splendeur.html)
-   [*140 contrevérités et diffamations à propos de Wikileaks et Julian
    Assange couramment relayées par la
    presse.*](https://www.legrandsoir.info/140-contreverites-et-diffamations-a-propos-de-wikileaks-et-julian-assange-couramment-relayees-par-la-presse.html)
-   [*La crucifixion de Julian Assange - Ce qui arrive à Assange devrait
    terrifier la presse (Truth
    Dig)*](https://www.legrandsoir.info/la-crucifixion-de-julian-assange-ce-qui-arrive-a-assange-devrait-terrifier-la-presse-truth-dig.html)
-   [*Pour le Prix Nobel de la paix à Julian Assange (The Irish
    Examiner)*](https://www.legrandsoir.info/pour-le-prix-nobel-de-la-paix-a-julian-assange-the-irish-examiner.html)
-   [*L’étrange silence de la gauche sur Julian
    Assange*](https://www.legrandsoir.info/l-etrange-silence-de-la-gauche-sur-julian-assange-mint-press-news.html)
-   [*Nous sommes Women Against Rape (Femmes Contre le Viol) et nous ne
    voulons pas que Julian Assange soit extradé (The
    Guardian)*](https://www.legrandsoir.info/nous-sommes-women-against-rape-et-nous-ne-voulons-pas-que-julian-assange-soit-extrade-the-guardian.html)
-   [*"Accusations suédoises" : Déposition de Julian Assange - 14/15
    novembre 2016 (texte
    complet)*](https://www.legrandsoir.info/accusation-suedoises-deposition-de-julian-assange-14-15-novembre-2016-texte-complet.html)
-   [*Julian Assange & l’affaire Suèdoise : Dépositions et témoignages à
    la police (Nordic News
    Network)*](https://www.legrandsoir.info/julian-assange-l-affaire-suedoise-depositions-et-temoignages-a-la-police-nordic-news-network.html)
-   "[*Un système meurtrier est en train de se créer sous nos
    yeux"*](https://www.legrandsoir.info/un-systeme-meurtrier-est-en-train-de-se-creer-sous-nos-yeux-republik.html)
-   [*De nouveaux courriels accablants montrent la corruption
    britannique dans l’affaire Julian
    Assange*](https://www.legrandsoir.info/de-nouveaux-courriels-accablants-montrent-la-corruption-britannique-dans-l-affaire-julian-assange-medium.html)

## Vidéo :

+ [Le scandale de l'affaire Assange avec Viktor Dedaj ... -
YouTube](https://www.youtube.com/watch?v=jG42Qi7q0vk)
