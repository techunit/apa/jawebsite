---
pagetitle=Noël
template=index
---
# Faites un cadeau à Julian Assange pour Noël !

## Ecrire à Julian Assange

Vous pouvez écrire une lettre à Julian Assange dans le cadre de la campagne d'envoi de lettres en masse organisée par le site [writejulian.com](https://writejulian.com/).


Avant d'écrire à Julian Assange, nous vous prions d'écrire au moins un courrier à un homme ou une femme politique et de vous engagez pour aider à défendre Julian Assange. Faites savoir à Julian les actions que vous avez entrepris pour informer les personnes capables d'intervenir en sa faveur et plébisciter une action de leur part.
Ecrivez à tous les acteurs politique afin qu'ils interviennent et accordent l'asile politique à Julian Assange.


```
Mr. Julian Assange
Prisoner #:A9379AY
HMP Belmarsh
Western way
London SE28 0EB
UK

```




![](img/letter.png)


Comment faire : 

+ Envoyez votre lettre exactement comme le modèle ci-dessus. Vous devez préciser le numéro de matricule prisonnier de Julian Assange ou sa date de naissance (03/07/1971).
+ Incluez votre nom complet et votre adresse au dos de l'enveloppe ou alors la lettre ne sera pas livrée.
+ Envoyez un court texte manuscrit que vous aurez écrit.
+ Inclure une action que vous avez entreprit pour protéger Julian (#ProtectJulian).
+ Inclure une feuille blanche avec une enveloppe adressée à votre nom (nom et prénom écrit au stylo, pas au crayon) afin que Julian vous réponde. Elle doit être timbrée pour [le Royaume Unis](https://www.royalmail.com/price-finder).
+ Envoyez uniquement du papier (lettre, photos, dessins).
+ Partagez la réponse de Julian avec le hashtag #WriteJulian (sauf s'il a demandé à ce que la lettre ne soit pas partagée ou que le courrier soit marqué avec « PRIVATE »).


Ce qu'il ne faut pas faire :

+ Ne pas envoyer de matériel susceptible d'être dangereux.
+ Ne pas envoyer une carte de voeux, une carte postale, un colis, un timbre non collé ou de l'argent.
+ Ne pas envoyer des livres ou des magazines.
+ N'envoyez pas trop de lettre où alors elles seront rejetées.
+ Ne publiez pas la réponse de Julian si elle est marquée « Private » ou s'il vous demande de ne pas la partager. 

Page original en anglais : [writejulian.com](https://writejulian.com/).
