#!/usr/bin/python3
# coding: utf-8

#import libweb as lw
import libswg as swg

import pypandoc
from dirsync import sync
from pathlib import Path
import sys
from bs4 import BeautifulSoup

# root folder path
path_root = Path.cwd()

# fix path
path_content = path_root / 'content'
path_themes = path_root / 'themes'
path_public = path_root / 'public'

# config path file
path_site_conf = path_root / 'site.conf'
path_menu_conf = path_root / 'menu.conf'

# welcoming message
print("STATIC WEBSITE BUILDING\n")

# loading site.conf
try:
  d_site_conf = swg.extractconfig(path_site_conf.read_text())
except:
  sys.exit(f'[error] unable to load {path_site_conf}')

# loading menu.conf
s_menu_conf = path_menu_conf.read_text()

# select theme
path_theme = path_themes / d_site_conf["theme"]
print("selected theme : {}".format(path_theme))

# loading templates
path_templates = path_theme
print(f'looking for templates in : {path_templates}')
t_path_template_file = path_templates.glob('*.html')
d_templates = {}
print("templates availables :")
for path_template_file in t_path_template_file:
  print(f' + {path_template_file}')
  try:
    s_template = path_template_file.read_text()
  except:
    sys.exit('[error] unable to load {path_template_file}')
  d_templates[path_template_file.stem] = s_template

# loading content of each md file
d_content = {}
t_path_content_file = path_content.glob('*.md')
print("content : ")
for path_content_file in t_path_content_file:
  print(f' + {path_content_file}')
  try:
    s_data = path_content_file.read_text()
  except:
    sys.exit(f'[error] unable to load {path_content_file}')
  try:
    s_header, s_content = swg.parsemd(s_data)
  except:
    sys.exit(f'[error] unable to parse {path_content_file}')
  d_tmp = swg.extractconfig(s_header)
  # convert markdown to html
  s_html = pypandoc.convert_text(s_content, 'html', format='md')
  # box the things to box
  s_html = swg.boxit(s_html)
  d_tmp['content'] = s_html
  d_content[path_content_file.stem] = d_tmp

# manage custom emoji
# TODO

# home link
s_home_link = Path(d_site_conf["homefile"]).with_suffix('.html')
print(f'home link : {s_home_link}')

# cleaning .html files in public/ folder
print(f'cleaning {path_public}')
t_file_to_delete = path_public.glob('*.html')
for file_to_delete in t_file_to_delete:
  file_to_delete.unlink()

# for each md file
for page in d_content:
  print(f"building page : {page}")
  path_current_file = Path(page).with_suffix('.html')

  d_page = d_content[page]
  s_template = d_templates[d_page["template"]]

  d_tmp = {}
  d_tmp.update(d_page)
  d_tmp.update(d_site_conf)
  d_tmp["menu"]=swg.buildmenu(s_menu_conf,d_content,page)

  # if not home add generic.css in the page
  if page == Path(d_site_conf["homefile"]).stem : 
    print(f'the page {page} is the home page')
    d_tmp["morecss"]=""
  else :
    print(f'the page {page} is not the home page, adding css')
    d_tmp["morecss"]='<link href="css/generic.css" rel="stylesheet" type="text/css"/>'


  # generate html page
  s_html = swg.usetemplate(s_template,d_tmp)
  # s_html = BeautifulSoup(s_html,'html.parser').prettify()
  path_current_file_dest = path_public / path_current_file
  path_current_file_dest.write_text(s_html)

#sync the other stuff from the theme to the public folder
sync(path_theme / 'css', path_public / 'css', 'sync')
sync(path_theme / 'js', path_public / 'js', 'sync')
sync(path_theme / 'img', path_public / 'img', 'sync')
sync(path_content / 'img', path_public / 'img', 'sync')
