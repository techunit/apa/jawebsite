# A static webpage builder in python

## What is it ?

It's a static webpage builder. In short : It takes the markdown files in the folder `content/`, it used a template in the `themes/` folder, it builds everthing and put the static website in the folder `public/`.

## Files & Folders of the project

+ `content/`: markdown files (the actual content of the pages) are stored here
  + `img/`: contain all the picture you want to see in yours pages.
+ `public/` : Once build, the website can be find there, it is automatically generated. Modification here will be ereased as soon as someone rebuild the website.
+ `scripts/` : python files that build the site
  + `libswg.py` : it's a bunch of python function called by `main.py`.
  + `main.py` : Script to be called to generate the website, it calls the `libswg.py` files for most of his functions.
+ `themes/` : contains the templates to generates the website. `css/`, `img/`, `js/` folders are copied as is in the `public/` folder.
  + `modern/` : a theme, it's a fancy word to say that it contains (almost) all the html and the css you'll see in the generated pages.
  + `index.html` : a template
+ `build.sh` : just a bash script that call the `main.py` script. It simply allows to build the website from the root of the folder by running `./build.sh`
+ `readme.md` : This file.
+ `site.conf` : A configuration file to set up global parameters such as the name of the website or the theme to use.
+ `menu.conf` : a configuration file to set up the menu

##  Usage

### build the static website

run `./build.sh` or `python3 scripts/main.py`

### publish modification online

run `./git_add-commit-push.sh "my commit"` or `git add . && git commit -m "my commit" && git push && git checkout publish-prod && git merge master && git push && git checkout master`

### get remote md files

run `./get_remote_markdown.sh`

### add/modify a webpage

#### header

+ create `mypage.md` in the `content\` directory
+ add at the top of the `mypage.md` :

```
---
pagetitle=My Page title
template=index

```

+ `pagetitle` is the title of your page, it will be displayed in the menu on the left of the page.
+ `template` is the template used to build the page. At the moment the theme "basic" got only 2 templates `front` and `generic`. With the theme "basic" you probably want to build every pages except the home page with the template `generic`.

#### content

After the header you can simply write the content of your page using markdown.
You can also add html if you want but be careful, it could break the page.

To add a image you can add the markdown syntax for it `![](img/myimage.png)` and simply add `myimage.png` in the folder `content\img\`.

Also, the following syntax allow at the moment to put each h2 title (##) and its following in little boxes :

```
---

---

## Title

Content

## Title

Content

---

---
```

## Notes

### build prerequisites
+ `python3`
+ `pypandoc` used to convert markdown into html. [web page](https://pypi.org/project/pypandoc/)
+ `dirsync` used to sync css, fonts, img between themes/basic/ and public/ [webpage](https://pypi.org/project/dirsync/)

note : I have no idea if that can run on windows, probably ? Anyway, I use xubuntu 18.04.

versions used :
+ python 3.6.9
+ pypandoc 1.5
+ dirsync 2.2.5

everything might work with other version but I don't know, if you use python3 it should be fine.
