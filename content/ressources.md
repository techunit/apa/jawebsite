---
pagetitle=Ressources
template=index
---
# Le web en parle
## Média Audio
### "Le futur président américain aura-t-il la peau de Julian Assange ?" - France Culture
<iframe src="https://www.franceculture.fr/player/export-reecouter?content=56edfc89-2dbd-4c43-9a4a-4dab6388f622" width="481" frameborder="0" scrolling="no" height="137"></iframe>

## Média Vidéo
### "Julian Assange n’est accusé de rien !" Interview de Viktor Dedaj - POUR .PRESS
<iframe width="560" height="315" src="https://www.youtube.com/embed/bbO_LNTRF8U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Groupes Facebook
### Toute la France avec Assange - French Action 4 Assange
[https://www.facebook.com/groups/1662956313803099/](https://www.facebook.com/groups/1662956313803099/)

### Assange, l'Ultime Combat
[https://www.facebook.com/groups/Assange.Ultime.Combat/](https://www.facebook.com/groups/Assange.Ultime.Combat/)
