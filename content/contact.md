---
pagetitle=Contact
template=index
---

# Contact Presse

Vous êtes journaliste ? Vous souhaitez en savoir plus sur l'affaire ? Vous souhaitez rédiger un article sur l'affaire Julian Assange ?

Vous pouvez nous contacter :

+ téléphone :  [+33 7 53 79 69 62](tel:+33753796962)
+ e-mail :  [journalistepourlapaix@protonmail.com](mailto:journalistepourlapaix@protonmail.com)
